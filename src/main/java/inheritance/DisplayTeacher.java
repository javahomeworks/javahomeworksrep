package inheritance;

public class DisplayTeacher {

	public static void main(String[] args) {
		Teacher teacher = new Teacher();
		teacher.does();
		
		teacher = new MathTeacher();
		teacher.does();
		MathTeacher mathteacher = (MathTeacher) teacher;
		
		teacher = new ChemistryTeacher();
		teacher.does();
		
		try {
			mathteacher = (MathTeacher) teacher;
		}
		catch (ClassCastException e) {
			System.out.println("Casting from " + teacher.getClass().getSimpleName() + " to MathTeacher is not possible");
		}
		
		if(teacher instanceof MathTeacher) System.out.println("Teacher is Math teacher");
		else if (teacher instanceof ChemistryTeacher) System.out.println("Teacher is Chemistry teacher");
	}

	
	
}

class Teacher {
	private String designation = "Teacher";
	private String collegeName = "Science";
	protected String mainSubject;
	
	public void does() {
		System.out.println("Teaching");
	}
}

class ChemistryTeacher extends Teacher {
	
	public ChemistryTeacher() {
		this.mainSubject = "Chemistry";
	}
	
	@Override
	public void does() {
		System.out.println("Teaching Chemistry");
	}
	

class MathTeacher extends Teacher {
	
	public MathTeacher() {
		this.mainSubject = "Mathematics";
		}
	
	@Override
	public void does() {
		System.out.println("Teaching Mathematics");
	}
	
	}
}

	