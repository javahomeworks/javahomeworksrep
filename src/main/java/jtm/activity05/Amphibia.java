package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Transport {

	private Ship ship;
	private Vehicle vehicle;

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		ship = new Ship(id, sails);
		vehicle = new Vehicle(id, consumption, tankSize, wheels);

	}

	@Override
	public String move(Road road) {
		String answer = "";
		if (road instanceof WaterRoad) {
			answer = ship.move(road).replace("Ship", "Amphibia");
		} else {
			answer = super.move(road).replace("moving", "driving") + " with " + vehicle.number_of_wheels + " wheels";
		}
		return answer;

	}

}
