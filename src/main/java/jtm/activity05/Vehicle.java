package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Vehicle extends Transport {

	protected int number_of_wheels;

	public Vehicle(String id, float consumption, int tankSize, int wheels) {
		super(id, consumption, tankSize);
		this.number_of_wheels = wheels;

	}

	@Override
	public String move(Road road) {
		String answer = "";
		if ((road instanceof WaterRoad)) {
			answer = "Cannot drive on " + road.toString();
		} else {
			String moveStatus = super.move(road);
			if (moveStatus.contains("moving")) {
				answer = moveStatus.replace("moving", "driving") + " with " + this.number_of_wheels + " wheels";
			}
		}
		return answer;
	}

}
