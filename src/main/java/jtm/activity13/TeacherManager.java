package jtm.activity13;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mysql.cj.xdevapi.Result;

public class TeacherManager {

	protected Connection conn;
	public Statement stmt;
	
	

	public TeacherManager() {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// user = "student"
		// pass = "Student007"
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods
	
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "root","");
			stmt = conn.createStatement();
			conn.setAutoCommit(false);
			
		} catch (Exception e) {
			System.err.println(e);
		}

}
	//do as in lecture example, //create another database, don`t pass database name
	// use the url from jdbc exercise, user = "root", pass = ""
	

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher(int id) {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"
		
		//SELECT * FROM database_name.table_name
		//create new Teacher object! -> constructor
		//get result with constructing object
		//teacher = new Teacher (result.getInt(1), result.getString(2), result.getString(3));
		//use prepared statement

		Teacher teacher = new Teacher();
		
		try {
			String sql = "SELECT * FROM database_activity.teacher WHERE id LIKE ?";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			
			ResultSet result = preparedStatement.executeQuery();
		
		while (result.next()) {
			teacher = new Teacher (result.getInt(1), result.getString(2), result.getString(3));
		}

		}catch (Exception e) {
			e.printStackTrace();
		}	
		return teacher;
		}
	

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		
		//create objects of teachers, add them to list
		//preparedStatement.setString(1, "%" + firstName + "%"); - partial match search
		//execute query
		
		
		List<Teacher> listOfTeachers = new ArrayList<Teacher>();
		Teacher teacher = new Teacher();
		
		try {
		String sql = "SELECT * FROM database_activity.teacher WHERE firstname LIKE ? and lastname LIKE ?";
		PreparedStatement preparedStatement = conn.prepareStatement(sql);
		preparedStatement.setString(1, "%" + firstName + "%");
		preparedStatement.setString(2, "%" + lastName + "%");
		
		ResultSet rs = preparedStatement.executeQuery();
		while (rs.next()) {	
			teacher = new Teacher(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"));
			listOfTeachers.add(teacher);		
		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listOfTeachers;

	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.
		
		//create connection, execute insert Teacher with first and last names
		//commit changes
		
		try {
			String sql = "INSERT INTO database_activity.teacher (firstname, lastname) VALUES (?, ?)";
			PreparedStatement insertStatement = conn.prepareStatement(sql);
			insertStatement.setString(1, firstName);
			insertStatement.setString(2, lastName);
			int count = insertStatement.executeUpdate();
			
			if (count > 0) {
				conn.commit();
				return true;
			} else {
				conn.rollback();
				return false;
			}
		}
		 catch (SQLException e) {
			e.printStackTrace();			
		}
		 return false;
	
	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		//get all values of teacher, execute sql querry
		try {
			String sql = "INSERT INTO database_activity.teacher (id, firstname, lastname) VALUES (?, ?, ?)";
			PreparedStatement insertStatement = conn.prepareStatement(sql);
			insertStatement.setInt(1, teacher.getId());
			insertStatement.setString(2, teacher.getFirstName());
			insertStatement.setString(3, teacher.getLastName());
			int count = insertStatement.executeUpdate();
			
			if(count > 0) {
			conn.commit();
			return true;
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
		e.printStackTrace();
	}
		return false;
}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher(Teacher teacher) {
		boolean status = false;

		// TODO #6 Write an sql statement that updates teacher information.	
		//update teacher, in the same way as insert, if updated return true
		try {
			String sql = "UPDATE database_activity.teacher SET firstname = ?, lastname = ? WHERE id = ?";
			PreparedStatement updateStatement = conn.prepareStatement(sql);
			updateStatement.setInt(3, teacher.getId());
			updateStatement.setString(1, teacher.getFirstName());
			updateStatement.setString(2, teacher.getLastName());
			int count = updateStatement.executeUpdate();
			
			if(count > 0) {
				conn.commit();
				return true;
			} else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) {
		// TODO #7 Write an sql statement that deletes teacher from database.
		//write delete querry 
		boolean result = false;
		
		try {
			String sql= "DELETE FROM database_activity.teacher WHERE id = ?";
			PreparedStatement deleteStatement = conn.prepareStatement(sql);
			deleteStatement.setInt(1, id);
			int count = deleteStatement.executeUpdate();
			
			if(count > 0) {
				conn.commit();
				return true;
			} else {
				conn.rollback();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public void closeConnecion() {
		// TODO Close connection to the database server and reset conn object to null
			try {
				if(conn != null)
					conn.close();
				conn = null;
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}
