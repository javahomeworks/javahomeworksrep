package jtm.activity08;

public class SimpleCalcException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5314968337642461176L;

	
	public SimpleCalcException(String string) {
		super(string);
	}
	
	public SimpleCalcException(String string, Throwable cause) {
		super(string, cause);
	}

	
}
