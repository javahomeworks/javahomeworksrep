package jtm.activity08;


// TODO implement basic mathematical operations with int numbers in range
// of [-10..+10] (including)
// Note that:
// 1. input range is checked using assertions (so if they are disabled, inputs can be any int)
// 2. outputs are always checked and exception is thrown if it is outside proper range

public class SimpleCalc {
	
	public SimpleCalc() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private static int min = -10;
	private static int max = 10;
	public int a, b;
	public static int result = 0;

	// TODO specify that method can throw SimpleCalcException
	public static int add(int a, int b) throws SimpleCalcException {
		// TODO implement adding operation
		SimpleCalc.validateInput(a, b);
		SimpleCalc.validateOutput(a, b, "+");	
		return a + b;
	}

	// TODO specify that method can throw SimpleCalcException
	public static int subtract(int a, int b) throws SimpleCalcException {
		// TODO implement subtract operation
		SimpleCalc.validateInput(a, b);
		SimpleCalc.validateOutput(a, b, "-");
		return a - b;
	}

	// TODO specify that method can throw SimpleCalcException
	public static int multiply(int a, int b) throws SimpleCalcException	{
		// TODO implement multiply operation
		SimpleCalc.validateInput(a, b);
		SimpleCalc.validateOutput(a, b, "*");
		return a * b;
	}

	// TODO specify that method can throw SimpleCalcException
	public static int divide(int a, int b) throws SimpleCalcException {
		// TODO implement divide operation
		SimpleCalc.validateInput(a, b);
		SimpleCalc.validateOutput(a, b, "/");
		return a / b;
	}

	// TODO Validate that inputs are in range of -10..+10 using assertions
	// Use following messages for assertion description if values are not in
	// range:
	// "input value a: A is below -10"
	// "input value a: A is above 10"
	// "input value b: B is below -10"
	// "input value b: B is above 10"
	// "input value a: A is below -10 and b: B is below -10"
	// "input value a: A is above 10 and b: B is below -10"
	// "input value a: a is below -10 and b: B is above 10"
	// "input value a: a is above 10 and b: B is above 10"
	//
	// where: A and B are actual values of a and b.
	//
	// hint:
	// note that assert allows only simple boolean expression
	// (i.e. without &, |, () and similar constructs).
	// therefore for more complicated checks use following approach:
	// if (long && complicated || statement)
	// assert false: "message if statement not fulfilled";
	//
	private static void validateInput(int a, int b) {
		
		if(a < min && b > min && b < max)
		assert false : "input value a: " + a + " is below -10";
		
		if (a > max && b > min && b < max) 
		assert false: "input value a: " + a + " is above 10";
		
		if(a > min && a < max && b < min)
		assert false: "input value b: " + b + " is below -10";
		
		if(a > min && a < max && b > max)
		assert false: "input value b: " + b + " is above 10";
		
		if(a < min && b < min && a < max && b < max)
		assert false: "input value a: " + a + " is below -10 and b: " + b + " is below -10";
		
		if(a > max && b < min && a > min && b < max)
		assert false: "input value a: " + a + " is above 10 and b: " + b + " is below -10";
		
		if(a < min && b > max && a < max && b> min)
		assert false: "input value a: " + a + " is below -10 and b: " + b + " is above 10";
		
		if(a > max && b > max && a > min && b > min)
		assert false:  "input value a: " + a + " is above 10 and b: " + b + " is above 10";
	}

	// TODO use this method to check that result of operation is also in
	// range of -10..+10.
	// If result is not in range:
	//     throw SimpleCalcException with message:
	//     "output value a oper b = result is above 10"
	//     "output value a oper b = result is below -10"
	//     where oper is +, -, *, /
	// Else:
	//     return result
	// Hint:
	// If division by zero is performed, catch original exception and create
	// new SimpleCalcException with message "division by zero" and add
	// original division exception as a cause for it.
	private static int validateOutput(int a, int b, String operation) throws SimpleCalcException {
	
			try {
				if (operation == "+") {
					result = a + b;
				} else if (operation == "-") {
					result = a - b;
				}else if (operation == "*") {
					result = a * b;
				} else if (operation == "/"  && !(a == 0 || b == 0 )) {
					result = a / b;	
				} else if (operation == "/" && a == 0 || b == 0) {
					result = a / b;
				}
			}catch (Exception e) {
					throw new SimpleCalcException("division by zero", e);
				}
			if (result > max)
				throw new SimpleCalcException("output value " + a + " " + operation + " " + b +  " = " +  result + " is above 10");
			if (result < min)
				throw new SimpleCalcException("output value " + a + " " + operation + " " + b +  " = " +  result + " is below -10");
			
			return result;
			}

}

