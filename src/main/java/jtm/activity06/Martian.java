package jtm.activity06;


public class Martian implements Humanoid, Alien, Cloneable {

	int weight;
	Object stomach;
	int food;


	public Martian() {
		weight = -1;
	}
	
	@Override
	public void eat(Object item) {
		
		if (!(this.stomach == null)) {
			return;
		} 
		stomach = item;
		if(item instanceof Human) {
			Human tmp = (Human) item;
			weight = weight + tmp.getWeight();
			tmp.killHimself();
		}	
		if(item instanceof Integer) {
			Integer tmp = (Integer) item;
			weight = weight + tmp;

		}  
		
		if(item instanceof Martian) {
			Martian tmp = (Martian) item;
			weight = weight + tmp.getWeight();
			tmp.killHimself();
		} 
	} 

	
	@Override
	public void eat(Integer food) {
		if (this.stomach == null) {
			this.stomach = food;
			weight = weight + (int) stomach;
		}				
	} 

	@Override
	public Object vomit() {

		if(stomach instanceof Integer && (Integer) this.stomach !=0) {
			Integer copyInt = (Integer) this.stomach;
			this.weight -= (Integer) this.stomach;
			
			this.stomach = null;
			return copyInt;
		} else if (stomach instanceof Integer) {
			return this.weight;
		}
		
		if(stomach == null)
			return null;
		else 
			weight = -1;
		
		Object copy = this.stomach;
		this.stomach = null;
		
		return copy;
	}
	
	@Override
	public Object vomit(Object object) {
			if(stomach instanceof Object && this.stomach != null) {
			object = this.stomach;
			this.weight = this.weight - (int) object;
			this.stomach = null;
			return object;
		} else if (stomach instanceof Object) {
			return this.weight;
		}
		if (stomach == null)
			return null;
		else
			weight = -1;
		
		Object copy = this.stomach;
		this.stomach = null;
		return copy;
	}

	@Override
	public String isAlive() {
		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {
		return isAlive();
	}

	@Override
	public int getWeight() {
		return weight;
	}
	
	@Override
    public Object clone() throws CloneNotSupportedException {	
		return clone(this);
}

	private Object clone (Object current) throws CloneNotSupportedException {
    // TODO implement cloning of current object
    // and its stomach
		if (current == null) {
			return null;
		}
		if(current instanceof Integer)
				return current;
		if(current instanceof Human) {
			Human tmpHuman = (Human) current;
			Human newHuman = new Human();
		//	set up weight, copy stomach, no need to eat
			newHuman.weight = tmpHuman.weight;
			newHuman.stomach = tmpHuman.stomach;
			return newHuman;
		}
		
		if (current instanceof Martian) {	
			Martian tmpMartian = new Martian();
			//clone stomach, copy weight
			tmpMartian.stomach = clone (((Martian) current).stomach);
			tmpMartian.weight = ((Martian) current).getWeight();
			return tmpMartian;
		}
		return current;
		
			
	}
		
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + weight + " [" + stomach + "]";
	}

}
