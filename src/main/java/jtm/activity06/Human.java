package jtm.activity06;

public class Human implements Humanoid {
	
	int stomach;
	int birthWeight;
	boolean isAlive;
	int weight;
	
	public Human () {
		stomach = 0;
		birthWeight = BirthWeight;
		isAlive = true;
		weight = 0;
	}
	
	
	
	@Override
	public void eat(Integer food) {
		// TODO Auto-generated method stub
		
		this.stomach = food;
		
	}

	
	// need to have only one method, return Integer, reduce weight by eaten item
	//stomach becomes empty, keep value of stomach, then stomach = 0, return the value of vomit at the end
	//4 lines of code
	@Override
	public Integer vomit() {
		// TODO Auto-generated method stub
		//this.weight = this.eatenItem;
	//	int currentEaten = this.eatenItem;
	//	this.eatenItem = 0;
		
		this.weight = this.stomach;
		int currentEaten = this.stomach;
		this.stomach = 0;
		return currentEaten;
	}

	@Override
	public String isAlive() {
		// TODO Auto-generated method stub
		if(isAlive)
			return "Alive";
		else
			return "Dead";
		
	}

	@Override
	public String killHimself() {
		// TODO Auto-generated method stub
		isAlive = false;
		return isAlive();
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return birthWeight + stomach;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" + " " + (birthWeight + stomach) +  " [" + stomach + "]";
	}

}
