package jtm.extra04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;

/*-
 * 
 * This class represents string tokenizer exercise.
 */
public class StringTokenizerExercise {

	public String[] splitString(String text, String delimiter) {

		// TODO # 1 Split passed text by given delimiter and return array with
		// split strings.
		// HINT: Use System.out.println to better understand split method's
		// functionality.

		String[] list = text.split(delimiter);
		for (String split : list)
			System.out.println(split);
		return list;
	}

	public List<String> tokenizeString(String text, String delimiter) {
		// TODO # 2 Tokenize passed text by given delimiter and return list with
		// tokenized strings.
		List<String> list = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(text, delimiter);
		String token = null;
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
			list.add(token);
		}
		return list;
	}

	private BufferedReader input;

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {
			String filepath = reader.readLine();
			String delim = reader.readLine();

			StringTokenizerExercise obj = new StringTokenizerExercise();
			List<Student> listStud = obj.createFromFile(filepath, delim);
			System.out.println(listStud.toString());

		} catch (IOException e) {
			System.out.println("Error occured!");
		}
	}

	public List<Student> createFromFile(String filepath, String delimiter) {

		// TODO # 3 Implement method which reads data from file and creates
		// Student objects with that data. Each line from file contains data for
		// 1 Student object.
		// Add students to list and return the list. Assume that all passed data
		// and
		// files are correct and in proper form.
		// Advice: Explore StringTokenizer or String split options.

		File studentsFile = new File(filepath);
		List<Student> studentsList = new ArrayList<Student>();

		try {
			input = new BufferedReader(new FileReader(studentsFile));
			String lineStr = "\n";

			String[] columns = new String[4];

			while ((lineStr = input.readLine()) != null) {

				StringTokenizer line = new StringTokenizer(lineStr, delimiter);

				columns[0] = (String) line.nextElement();
				columns[1] = (String) line.nextElement();
				columns[2] = (String) line.nextElement();
				columns[3] = (String) line.nextElement();

				Student student = new Student(Integer.valueOf(columns[0]), columns[1], columns[2],
						Integer.valueOf(columns[3]));

				student.setID(Integer.valueOf(columns[0]));
				student.setFirstName(columns[1]);
				student.setLastName(columns[2]);
				student.setPhoneNumber(Integer.valueOf(columns[3]));

				studentsList.add(student);
			}
		} catch (IOException e) {
			System.out.println("Error occured");
		}
		return studentsList;

	}
}


