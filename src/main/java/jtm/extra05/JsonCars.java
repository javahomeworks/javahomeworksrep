package jtm.extra05;

import org.json.*;


import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/*
 * {"cars":[{"color":"Yellow","year":1998,"price":3293,"model":"GMC"},
 * {"color":"Green","year":2017,"price":3763,"model":"Etox"}]}
 */

public class JsonCars {

	/*- TODO #1
	 * Implement method, which returns list of cars from generated JSON string
	 */

	public List<Car> getCars(String jsonString) {
		/*- HINTS:
		 * You will need to use:
		 * - https://stleary.github.io/JSON-java/org/json/JSONObject.html
		 * - https://stleary.github.io/JSON-java/org/json/JSONArray.html
		 * You will need to initialize JSON array from "cars" key in JSON string
		 */
		JSONObject fileObject = new JSONObject(jsonString);
			
		JSONObject carValue = fileObject.getJSONObject("cars");
		
		List<Car> cars = new ArrayList<Car>(); 
				
		JSONArray carItems = carValue.getJSONArray("cars");
		Iterator<Object> iterator = carItems.iterator();
		while(iterator.hasNext()) {
			JSONObject item = (JSONObject) iterator.next();
			Car car = new Car();
			car.setModel(item.getString("model"));
			car.setYear(item.getInt("year"));
			car.setColor(item.getString("color"));
			car.setPrice(item.getFloat("price"));
			cars.add(car);
		
		}	
		return cars;
			
}// just get corresponding value of the car, get json file, that starts with object, get array from atribute (cars)
	//for every car create new object with reference to class car, get all the values (price, model...), 
	//pass values to the object car, pass car to list of cars. list of cars is returning parameter of getCars

	
	/*- TODO #2
	 * Implement method, which returns JSON String generated from list of cars
	 */
	public String getJson(List<Car> cars) {
		/*- HINTS:
		 * You will need to use:
		 * - https://docs.oracle.com/javase/8/docs/api/index.html?java/io/StringWriter.html
		 * - http://static.javadoc.io/org.json/json/20180130/index.html?org/json/JSONWriter.html
		 * Remember to add "car" key as a single container for array of car objects in it.
		 */
			
		//opposite way, create json file, get elements (price, model..) from car (object) and
         //pass values to atributes in JSON
		//use JSONObject (stands for object), JSONArray (put elements in)	
		
		JSONObject carsObject = new JSONObject();
		JSONArray carsArray = new JSONArray();
	
		for(int i=0; i<cars.size(); i++) {
			Car car = cars.get(i);
			
			String model = car.getModel();
			Integer year = car.getYear();
			String color = car.getColor();
			Float price = car.getPrice();
		
			JSONObject carJ = new JSONObject();
			carJ.put("model", model);
			carJ.put("year", year);
			carJ.put("color", color);
			carJ.put("price", price);
					
			
			carsArray.put(carJ);
				
	}
		carsObject.put("cars", carsArray);
		
		return carsObject.toString();
	}

}