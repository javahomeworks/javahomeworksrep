package jtm.activity09;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */
public class Orders implements Iterator<Order> {

	
	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */
	
	List<Order> listOfOrders = new LinkedList<Order>();
	int currentOrder = -1;
	Order order = null;
	

	public Orders() {
	}
	

	public void add (Order item) {
		listOfOrders.add(item);		
	}
	
	public List<Order> getItemsList() {
		return listOfOrders;	
	}
	
	public Set<Order> getItemsSet() {
			
		Set<Order> setOfOrders = new TreeSet<Order>();
		sort();
		Order previous = null;
		int count = 0;
	
		String nameSet = "";
		String customersSet = "";
		Order newOrderSet = new Order(nameSet, customersSet, count);
		
		//if name is the same for 2 orders we need to sum the count of those orders and join the customers
	//	 When implementing getItemsSet() method, join all requests for the same item from different customers
	//	 * in following way: if there are two requests:
	//	 *  - ItemN: Customer1: 3
	//	 *  - ItemN: Customer2: 1
	//	 *  Set of orders should be:
	//	 *  - ItemN: Customer1,Customer2: 4
		//new object, then set, use for loop, go through every Order from list of orders, 
		//check the name, getItemSet
		
		if(listOfOrders.size() == 0)
			return setOfOrders;
		
		for(Order order : listOfOrders) {
				if(previous == null) {
				previous = order;
				count = previous.count;
				customersSet += previous.customer;
				continue;
			}
			
		if(previous.name.equals(order.name)) {
			count += order.count;
			nameSet = previous.name;
			customersSet += order.customer;
			
			newOrderSet.count = count;
			newOrderSet.name = nameSet;
			newOrderSet.customer = customersSet;
			setOfOrders.add(newOrderSet);
		}  else if (previous.name != listOfOrders.iterator().next().name) {
			previous.name = order.name;
		}	

		}	
	return setOfOrders;
	}

	

	public void sort() {
		Collections.sort(listOfOrders);
	}
	
	
	@Override
	public boolean hasNext() {
		if(listOfOrders.iterator().hasNext()) {
		return true;
		} else {
			return false;
		}
	}

	@Override
	public Order next() throws NoSuchElementException {
	//	 get next Order from Orders, throw NoSuchElementException if can't
		Iterator<Order> iterator = listOfOrders.iterator(); {
			if(listOfOrders.isEmpty())
				throw new NoSuchElementException();
			while (iterator.hasNext()) {	
				order = iterator.next();
				currentOrder += 1;	
			}		
		}
		return order;		
	}
	
	
	@Override
	public void remove() throws IllegalStateException {
		//remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
		if(listOfOrders.isEmpty())
			throw new IllegalStateException();
		else
			listOfOrders.remove(order);
}

	
	@Override
	public String toString() {
		return listOfOrders.toString();
	}


}
