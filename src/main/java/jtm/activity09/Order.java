package jtm.activity09;


import java.util.Objects;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order
 * - public int compareTo(Order order) — comparison implementation according to logic described below
 * - public boolean equals(Object object) — check equality of orders
 * - public int hashCode() — to be able to handle it in some hash... collection 
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"
 * 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */

public class Order implements Comparable<Order> {
	public static Object previous;
	String customer; // Name of the customer
	String name; // Name of the requested item
	int count; // Count of the requested items
	

	public Order (String orderer, String itemName, Integer count) {
		this.customer = orderer;
		this.name = itemName;
		this.count = count;
	}


	@Override
	public int compareTo(Order order) {
		// TODO Auto-generated method stub
//compare current with order? two variables to compare
// first compare item name, if they are equal, then check customer names, if they are the same, then we check count
		int comparedItemName = 0;
		int comparedCustomer = 0;
		int comparedCount = 0;
				
			comparedItemName = this.name.compareTo(order.name);
			if (comparedItemName < 0)
				comparedItemName = -1;
			else if (comparedItemName > 0)
				comparedItemName = 1;
			else
				comparedItemName = 0;

		
		if (comparedItemName == 0) {
				comparedCustomer = this.customer.compareTo(order.customer);
				if (comparedCustomer < 0)
					comparedCustomer = -1;
				else if (comparedCustomer > 0)
					comparedCustomer = 1;
				else
					comparedCustomer = 0;
			}

		if (comparedItemName == 0 && comparedCustomer == 0) {
				comparedCount = this.count - order.count;
			}

		
		if (comparedItemName != 0)
			return comparedItemName;
		else if (comparedCustomer != 0)
			return comparedCustomer;
		else
			return comparedCount;
	}
	
	
	@Override
	public boolean equals (Object object) {
		//need to consider this: orders with the same values should be considered equal
		//two variables to compare

		if ((object == null) || (object.getClass() != this.getClass())) {
			return false;
		}
		Order anotherOrder = (Order) object;
		return this.name == anotherOrder.name && Objects.equals(customer, anotherOrder.customer) && Objects.equals(count, anotherOrder.count);
	}
	

	@Override
	public int hashCode() {
		//need to consider this: orders with the same values should be considered equal 
				// call method from string
		return Objects.hash(name, customer, count);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		//"ItemName: OrdererName: Count"
		return this.name + ": " + this.customer + ": " + this.count;
	}

	
}
