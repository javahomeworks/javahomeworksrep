package jtm.extra03;

import java.util.stream.IntStream;
import java.math.BigInteger;
import java.util.Arrays;

public class PracticalNumbers {

	// TODO Read article https://en.wikipedia.org/wiki/Practical_number
	// Implement method, which returns practical numbers in given range
	// including
	public String getPracticalNumbers(int from, int to) {
		return Arrays.toString( IntStream.range( from, to + 1 ).filter( this::isPracticalNumber ).toArray() );
    }

    public int[] getPrimeFactors( int number ) {
        int[] factors = new int[100];
        int currentPrimeFactor = 2;
        int i = 0;
        while ( number >= Math.pow( currentPrimeFactor, 2 ) ) {
            if ( BigInteger.valueOf( number ).isProbablePrime( 50 ) ) {
                factors[i] = number;
                break;
            }
            if ( number % currentPrimeFactor == 0 ) {
                factors[i] = currentPrimeFactor;
                number = number / currentPrimeFactor;
                i++;
            } else {
                currentPrimeFactor = BigInteger.valueOf( currentPrimeFactor ).nextProbablePrime().intValue();
            }

        }
        return Arrays.stream( factors ).filter( n -> n != 0 ).toArray();
    }

    //1, 2, 4, 6, 8, 12, 16, 18, 20, 24, 28, 30, 32, 36, 40, 42, 48, 54, 56, 60, 64, 66, 72, 78, 80, 84, 88, 90, 96, 100, 104, 108, 112, 120, 126, 128, 132, 140, 144, 150
    public boolean isPracticalNumber( int number ) {
        if ( number == 1 || number == 2 ) return true;
        int[] primeFactors = getPrimeFactors( number );
        int[] distinctPrimeFactors = Arrays.stream( primeFactors ).distinct().toArray();
        if ( distinctPrimeFactors.length == 0 || distinctPrimeFactors[0] != 2 ) return false;
        for ( int i = 0; i < distinctPrimeFactors.length; i++ ) {
            int primeFactor = distinctPrimeFactors[i];

            int inequalityRightHandValue = 1 + sumOfDivisers( Arrays.stream( primeFactors ).filter( d -> d < primeFactor ).reduce( 1, ( val, n ) -> val * n ) );
            if ( primeFactor > inequalityRightHandValue ) {
                return false;
            }
        }
        return true;
    }

    private int sumOfDivisers( int number ) {
        return IntStream.range( 1, number + 1 ).filter( i -> number % i == 0 ).reduce( 0, Integer::sum );
    }

    }


