package jtm.activity04;

public class Road {
	
	private String from; // Start point
	private String to; // End point
	private int distance; // distance in km

	/*- TODO #1
	 * Select menu Source вЂ” Generate Constructor using Fields...
	 * and create constructor which sets from, to and distance
	 * values of the newly created object
	 */
	
	/**
	 * @param from
	 * @param to
	 * @param distance
	 */
	public Road(String from, String to, int distance) {
		this.from = from;
		this.to = to;
		this.distance = distance;
	}
	

	/*- TODO #2
	 * Create constructor without parameters, which sets empty
	 * values or 0 to all object properties
	 */
	
	public Road() {
		this.from = "";
		this.to = "";
		this.distance = 0;
	}


	/*- TODO #3
	 * Select menu: Source вЂ” Generate getters and Setters...
	 * and generate public getters/setters for distance, from and to
	 * fields
	 */
	
	public String getFrom() {
		return this.from;
	}


	public void setFrom(String from) {
		this.from = from;
	}


	public String getTo() {
		return this.to;
	}


	public void setTo(String to) {
		this.to = to;
	}


	public int getDistance() {
		return this.distance;
	}


	public void setDistance(int distance) {
		this.distance = distance;
	}



	/*- TODO #4
	 * Select menu: Source вЂ” Generate toString()...
	 * and implement this method, that it returns String in form:
	 * "From вЂ” To, 00km",
	 * where "From" is actual from point, "To" вЂ” actual to point and
	 * 00 is actual length of the road
	 * Note that вЂ” is not dash ("minus key" in jargon), but m-dash!
	 * See more at: https://en.wikipedia.org/wiki/Dash
	 */
	
	@Override
	public String toString() {
		//return "From " + from + " To " + to + ", " + distance + "km";
		return from + " \u2014 " + to + ", " + distance + "km";
	}
	
	//we added this to check if all is correct
	public static void main(String[] args) {
		Road myroad = new Road("Rīga", "Liepāja", 218);
		System.out.println(myroad.toString());
	}

	


}
