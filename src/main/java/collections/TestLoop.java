package collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class TestLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Book> bookSet = new ArrayList<Book>();
		bookSet.add(new Book('A', "J.R.Rowling", "Harry Potter", 1997));
		bookSet.add(new Book('A', "A.Deglavs", "Rīga", 1963));
		bookSet.add(new Book('B', "Tolkien", "Lord of The Rings", 1964));
		bookSet.add(new Book('B', "Fitzgerald", "Great Gatsby", 1928));

		
		System.out.println(bookSet);
		
		Map<String, Integer> categories = new TreeMap<String, Integer>();
		
		//define previous book
		Book previous = null;
		int count = 0;
		for(Book book : bookSet) {
			
			if(previous == null) {
				previous = book;
			continue;
		}
		if(previous.getCategory() == book.getCategory())
			count +=1;
			categories.add(String.previous.getCategory().count);
			
		}
	}

}


class Book {
	private char category;
	private String author;
	private String name;
	private int yearPublished;
	public int count;
	
	public Book(char category, String author, String name, int year) {
		// TODO Auto-generated constructor stub
		this.author = author;
		this.name = name;
		this.yearPublished = year;
	}
	
	public char getCategory() {
		return this.category;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.author + this.name;
	}
}